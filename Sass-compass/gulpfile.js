const gulp = require('gulp');
const minify= require ('gulp-minify');
const uglify = require ('gulp-uglify');
const autoprefixer = require ('gulp-autoprefixer');


gulp.task('minify', ()=>
	gulp.src('./src/*.js')
		.pipe(minify())
		.pipe(gulp.dest('prod'))
);

gulp.task('uglify', ()=>
	gulp.src('./src/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('prod'))
);

gulp.task('prefix', function(){
	gulp.src('./*.css')
	.pipe(autoprefixer("last 2 versions"))
	.pipe(gulp.dest('/'))

});

